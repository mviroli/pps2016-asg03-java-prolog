# README: Assignment 3 (PPS) #

## Setup ##

You need to add TuProlog as a dependency of the project. To do so, click menu ```File -> Project Structure```, then ```Modules``` page, select the ```Dependencies``` tab for the project module (```pps2016-asg03-java-prolog```), and add ```2p.jar``` as a dependency by ```[+] -> Jars or directories```.